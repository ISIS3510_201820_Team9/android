package edu.uniandes.aire;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.navigation.Navigation;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetallesFragment extends Fragment implements PermissionsListener {

    private FirebaseAnalytics fbAnalytics;
    public static final String TAG = "MyTag";
    FragmentManager fragmentManager;
    String locationProvider = LocationManager.GPS_PROVIDER;
    NivelAireInfoFragment newFragment = new NivelAireInfoFragment();
    Location gpspoint;
    boolean connected = false;
    PermissionsManager permissionsManager = new PermissionsManager(this);
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            gpspoint = location;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };
    LocationManager locationManager;

    public DetallesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detalles, container, false);
        getActivity().setTitle("Detalles de la zona");
        fragmentManager = getActivity().getSupportFragmentManager();
        ImageButton btn = (ImageButton) view.findViewById(R.id.imageButton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_nivelAireInfoFragment);
                newFragment.show(fragmentManager, "dialog");
            }
        });
        fbAnalytics = FirebaseAnalytics.getInstance(getActivity());
        return view;
    }
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
        if (MySingleton.getInstance(getContext()).getRequestQueue() != null) {
            MySingleton.getInstance(getContext()).getRequestQueue().cancelAll(TAG);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!connected){
            Snackbar snackbar = Snackbar
                    .make(getView(), "Reintentar la conexion", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_detallesFragment);
                        }
                    });
            snackbar.show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Button refrescar = (Button)getView().findViewById(R.id.but_refrescar);
        refrescar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_detallesFragment);
            }
        });
        Location lastKnownLocation = null;
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsManager = new PermissionsManager(this);
                permissionsManager.requestLocationPermissions(getActivity());
            }if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
                lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
            }
            if (lastKnownLocation!=null){
                gpspoint = lastKnownLocation;
            }
        }
        ConnectivityManager cm =(ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork!=null&&activeNetwork.isConnected()) {
            connected = true;
//            AsyncTask<Void, Void, String[]> execute = new RetrieveValues().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            AsyncTask<String, Void, String[]> execute = new RetrieveValues(new OnEventListener() {
                @Override
                public void onSuccess(String[] strings) {
                    TextView textoVelViento = (TextView)getView().findViewById(R.id.velviento_value);
                    textoVelViento.setText(strings[9]);
                    TextView textoPrecipitacion = (TextView)getView().findViewById(R.id.precipitacion_value);
                    textoPrecipitacion.setText(strings[11]);
                    TextView textoTemperatura = (TextView)getView().findViewById(R.id.tempera_value);
                    textoTemperatura.setText(strings[12]+"º");
                }
            }).execute("Kennedy");
        } else {
            connected = false;
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, "http://iboca.ambientebogota.gov.co/mod/IBOCA/user/h_iboca.json", null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray features = response.getJSONArray("features");
                            boolean encontro = false;
                            LatLng bottomLeft = null;
                            LatLng upperLeft = null;
                            LatLng bottomRight = null;
                            LatLng upperRight = null;
                            LatLng prueba;
                            if (gpspoint!=null){
                                prueba = new LatLng(gpspoint.getLatitude(),gpspoint.getLongitude());
                            }else {
                                prueba = new LatLng(4.6018736,-74.06613474);
                            }
                            List<LatLng> polygon = new ArrayList<>();
                            for (int i = 135; i < features.length(); i++) {
                                JSONObject jsonObject = features.getJSONObject(i);
                                JSONObject geometry = jsonObject.getJSONObject("geometry");
                                String color = jsonObject.getJSONObject("properties").getString("color");
                                String nivelContaminante = jsonObject.getJSONObject("properties").getString("valor_contaminante");
                                String calidadAire = jsonObject.getJSONObject("properties").getString("calidad_aire");
                                String localidad = jsonObject.getJSONObject("properties").getString("localidad");
                                String fecha = jsonObject.getJSONObject("properties").getString("fecha");
                                JSONArray coordinates = geometry.getJSONArray("coordinates");
                                JSONArray jsonArray = coordinates.getJSONArray(0);
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                                response = null;
                                jsonArray = null;
                                jsonObject = null;
                                geometry = null;
                                coordinates = null;
                                for (int j = 0; j < jsonArray1.length(); j++) {
                                    polygon.add(new LatLng(jsonArray1.getJSONArray(j).getDouble(1), jsonArray1.getJSONArray(j).getDouble(0)));
                                }
                                for (int j = 0; j < polygon.size()&&!encontro; j++) {
                                    if (bottomLeft == null){
                                        bottomLeft = polygon.get(j);
                                    }
                                    if (bottomRight == null){
                                        bottomRight = polygon.get(j);
                                    }
                                    if (upperLeft == null){
                                        upperLeft = polygon.get(j);
                                    }
                                    if (upperRight == null){
                                        upperRight = polygon.get(j);
                                    }
                                    if(polygon.get(j).getLongitude()<bottomLeft.getLongitude()||polygon.get(j).getLatitude()<bottomLeft.getLatitude()){
                                        bottomLeft = polygon.get(j);
                                    }
                                    if (polygon.get(j).getLongitude()>bottomRight.getLongitude()||polygon.get(j).getLatitude()<bottomRight.getLatitude()){
                                        bottomRight = polygon.get(j);
                                    }
                                    if (polygon.get(j).getAltitude()<upperLeft.getLongitude()||polygon.get(j).getLatitude()>upperLeft.getLatitude()){
                                        upperLeft = polygon.get(j);
                                    }
                                    if (polygon.get(j).getLongitude()>upperRight.getLongitude()||polygon.get(j).getLatitude()>upperRight.getLatitude()){
                                        upperRight = polygon.get(j);
                                    }
                                }
                                polygon = new ArrayList<>();
                                if (prueba.getLatitude()>=bottomLeft.getLatitude()&&prueba.getLatitude()<=upperLeft.getLatitude()||prueba.getLatitude()>=bottomRight.getLatitude()&&prueba.getLatitude()<=upperRight.getLatitude()){
                                    if (prueba.getLongitude()>=bottomLeft.getLongitude()&&prueba.getLongitude()>=upperLeft.getLongitude()||prueba.getLongitude()<=bottomRight.getLongitude()&&prueba.getLongitude()<=upperRight.getLongitude()){
                                        encontro = true;
                                        fbAnalytics.setUserProperty("last_place", localidad);
                                        Bundle event = new Bundle();
                                        event.putString("last_place", localidad);
                                        event.putInt("contaminant_level",Integer.parseInt(nivelContaminante));
                                        fbAnalytics.logEvent("nivel_localidad", event);
                                        if(localidad.compareTo("USAQUEN")==0){
                                            ImageView img_ciudad =(ImageView)getView().findViewById(R.id.imageView6);
                                            img_ciudad.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                            img_ciudad.setImageResource(R.mipmap.usaquen_city);
                                        }
                                        if(localidad.compareTo("TUNJUELITO")==0){
                                            ImageView img_ciudad =(ImageView)getView().findViewById(R.id.imageView6);
                                            img_ciudad.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                            img_ciudad.setImageResource(R.mipmap.tunjuelito_city);
                                        }
                                        if(localidad.compareTo("TEUSAQUILLO")==0){
                                            ImageView img_ciudad =(ImageView)getView().findViewById(R.id.imageView6);
                                            img_ciudad.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                            img_ciudad.setImageResource(R.mipmap.teusaquillo_city);
                                        }
                                        if(localidad.compareTo("SANTA FE")==0){
                                            ImageView img_ciudad =(ImageView)getView().findViewById(R.id.imageView6);
                                            img_ciudad.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                            img_ciudad.setImageResource(R.mipmap.santafe_city);
                                        }
                                        if(localidad.compareTo("CANDELARIA")==0){
                                            ImageView img_ciudad =(ImageView)getView().findViewById(R.id.imageView6);
                                            img_ciudad.setScaleType(ImageView.ScaleType.CENTER_CROP);
                                            img_ciudad.setImageResource(R.mipmap.candelaria_city);
                                        }
                                        TextView textoCalidad = (TextView)getView().findViewById(R.id.valor_contaminante);
                                        textoCalidad.setText(calidadAire);
                                        RatingBar ratingBar = (RatingBar)getView().findViewById(R.id.ratingBar);
                                        ratingBar.setRating(6-Integer.parseInt(nivelContaminante));
                                        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
                                        stars.getDrawable(2).setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
                                        TextView textoTiempo = (TextView)getView().findViewById(R.id.valor_tiempo_de_datos);
                                        textoTiempo.setText(fecha);
                                        TextView textoLocalidad = (TextView)getView().findViewById(R.id.valor_localidad);
                                        textoLocalidad.setText(localidad);
                                    }
                                }
                                upperLeft=new LatLng();
                                upperRight=new LatLng();
                                bottomLeft=new LatLng();
                                bottomRight=new LatLng();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        System.out.print("Error");
                    }
                });
        jsonObjectRequest.setTag(TAG);
        MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }
}