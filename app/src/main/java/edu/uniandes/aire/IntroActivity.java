package edu.uniandes.aire;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;

public class IntroActivity extends AppIntro {
    OnBoard1Fragment onBoard1Fragment = new OnBoard1Fragment();
    OnBoard2Fragment onBoard2Fragment = new OnBoard2Fragment();
    OnBoard3Fragment onBoard3Fragment = new OnBoard3Fragment();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        addSlide(onBoard1Fragment);
        addSlide(onBoard2Fragment);
        addSlide(onBoard3Fragment);

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        Intent intent = new Intent(this, MainActivity.class); // Call the AppIntro java class
        startActivity(intent);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        Intent intent = new Intent(this, MainActivity.class); // Call the AppIntro java class
        startActivity(intent);
    }
}
