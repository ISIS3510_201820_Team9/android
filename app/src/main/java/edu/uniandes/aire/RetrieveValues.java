package edu.uniandes.aire;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class RetrieveValues extends AsyncTask<String,Void,String[]> {
    private OnEventListener mCallBack;
    public RetrieveValues(OnEventListener callback){
        mCallBack = callback;
    }
    @Override
    protected String[] doInBackground(String... strings) {
        try {
            Document doc = Jsoup.connect("http://201.245.192.252:81/DynamicTable.aspx?G_ID=53").userAgent(".userAgent(\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21\")").get();
            Element kenedy = doc.getElementsMatchingText(strings[0]).get(17);
            kenedy.child(0).remove();
            Elements children = kenedy.children();
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i).text().isEmpty()){
                    children.get(i).appendText("null");
                }
            }
            String string = children.text().replace(",",".");;
            String[] split = string.split(" ");
            return split;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String[0];
    }

    @Override
    protected void onPostExecute(String[] strings) {
        super.onPostExecute(strings);
        if(strings.length!=0){
            mCallBack.onSuccess(strings);
        }
    }
}
