package edu.uniandes.aire;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.FillExtrusionLayer;
import com.mapbox.mapboxsdk.style.layers.FillLayer;
import com.mapbox.mapboxsdk.style.layers.HeatmapLayer;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import androidx.navigation.Navigation;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.heatmapOpacity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment implements OnMapReadyCallback, PermissionsListener, LocationEngineListener {

    private MapView mapView;
    private MapboxMap mapboxMap;
    public static final String TAG = "MyTag";
    PermissionsManager permissionsManager = null;
    LocationEngine locationEngine = null;
    boolean connected = false;
    private Location originLocation;

    public MapaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mapa, container, false);
        getActivity().setTitle("Mapa de contaminantes");
        ConnectivityManager cm =(ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this::onMapReady);
        if (activeNetwork!=null&&activeNetwork.isConnected()){
            connected = true;
        } else {
            connected = false;
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        if (locationEngine != null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationEngine.requestLocationUpdates();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (locationEngine != null) {
            locationEngine.removeLocationUpdates();
        }
        if (MySingleton.getInstance(getContext()).getRequestQueue() != null) {
            MySingleton.getInstance(getContext()).getRequestQueue().cancelAll(TAG);
        }
        mapView.onStop();
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.62490, -74.16135))
                .title("Kennedy")
                .snippet("Cr 86 No 40-55 Sur")
        );
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.63187, -74.11757))
                .title("Puente Aranda")
                .snippet("Cr 65 No 10-95")
        );
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.62529, -74.06724))
                .title("MinAmbiente")
                .snippet("CL 37 No. 8 - 40")
        );
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.65837, -74.08400))
                .title("Centro de Alto Rendimiento")
                .snippet("Cl 63 No 47-06")
        );
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.78374, -74.04417))
                .title("Guaymaral")
                .snippet("Auto Norte KM 13")
        );
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.76251, -74.09343))
                .title("Suba")
                .snippet("Ave Corpas Km 13 / KR 111 157-45")
        );
        mapboxMap.addMarker(new MarkerOptions()
                .position(new LatLng(4.5725, -74.0836))
                .title("San Cristobal")
                .snippet("Carrera 2 Este No. 12-78 Sur")
        );
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, "http://iboca.ambientebogota.gov.co/mod/IBOCA/user/h_iboca.json", null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray features = response.getJSONArray("features");
                            List<LatLng> polygon = new ArrayList<>();
                            for (int i = 0; i < features.length(); i++) {
                                JSONObject jsonObject = features.getJSONObject(i);
                                JSONObject geometry = jsonObject.getJSONObject("geometry");
                                String color = jsonObject.getJSONObject("properties").getString("color");
                                JSONArray coordinates = geometry.getJSONArray("coordinates");
                                JSONArray jsonArray = coordinates.getJSONArray(0);
                                JSONArray jsonArray1 = jsonArray.getJSONArray(0);
                                for (int j = 0; j < jsonArray1.length(); j++) {
                                    polygon.add(new LatLng(jsonArray1.getJSONArray(j).getDouble(1), jsonArray1.getJSONArray(j).getDouble(0)));
                                }
                                mapboxMap.addPolygon(new PolygonOptions()
                                        .addAll(polygon)
                                        .fillColor(Color.parseColor(color)));
                                polygon.clear();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        System.out.print("Error");
                    }
                });
        MySingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
        permissionsManager = new PermissionsManager(this);
        locationEngine = new LocationEngineProvider(getContext()).obtainBestLocationEngineAvailable();
        if (PermissionsManager.areLocationPermissionsGranted(getContext())) {

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
        locationEngine.activate();
        locationEngine.addLocationEngineListener(this);
        LocationComponent locationComponent = mapboxMap.getLocationComponent();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationComponent.activateLocationComponent(getContext());
        locationComponent.setLocationComponentEnabled(true);

    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onConnected() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        enableLocationComponent();
        locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationEngine.requestLocationUpdates();
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!connected){
            Snackbar snackbar = Snackbar
                    .make(getView(), "Reintentar la conexion", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_mapaFragment);
                        }
                    });
            snackbar.show();
        }
    }

    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent() {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(getContext())) {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(getContext());
            locationComponent.setLocationComponentEnabled(true);

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }
}
