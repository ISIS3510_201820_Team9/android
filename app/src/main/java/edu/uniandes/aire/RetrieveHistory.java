package edu.uniandes.aire;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class RetrieveHistory extends AsyncTask<String,Void,String[]> {
    private OnEventListener mCallBack;
    public RetrieveHistory(OnEventListener callback){
        mCallBack = callback;
    }
    @Override
    protected String[] doInBackground(String... strings) {
        try {
            Document doc = Jsoup.connect("http://201.245.192.252:81/OnlineC.aspx?ST_ID=9;0;GRID").userAgent(".userAgent(\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21\")").get();
            Element children = doc.getElementById("C1WebGrid1");
            Element element = children.children().get(1);
            element.child(0).remove();
            element.child(0).remove();
            String[] split = new String[10];
            for (int i = 0; i < element.childNodeSize()-1; i++) {
                Element child = element.child(i);
                split[i] = child.child(0).text();
                String temp = "";
                if (strings[0].compareTo("PM10")==0){
                    temp =  child.child(1).text().replace(",",".");
                }
                if (strings[0].compareTo("PM25")==0){
                    temp =  child.child(2).text().replace(",",".");
                }
                if (strings[0].compareTo("CO")==0){
                    temp =  child.child(3).text().replace(",",".");
                }
                if (strings[0].compareTo("O3")==0){
                    temp =  child.child(15).text().replace(",",".");
                }
                if (strings[0].compareTo("NO2")==0){
                    temp =  child.child(5).text().replace(",",".");
                }
                if (strings[0].compareTo("SO2")==0){
                    temp =  child.child(7).text().replace(",",".");
                }
                if (temp.isEmpty()){
                    temp="0";
                }
                split[5+i] = temp;
            }
            return split;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String[0];
    }
    @Override
    protected void onPostExecute(String[] strings) {
        super.onPostExecute(strings);
        if(strings.length!=0){
            mCallBack.onSuccess(strings);
        }
    }
}
