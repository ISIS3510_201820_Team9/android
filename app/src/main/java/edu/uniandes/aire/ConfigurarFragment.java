package edu.uniandes.aire;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigurarFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    public ConfigurarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_configurar, container, false);
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        Switch boolWearable = (Switch) view.findViewById(R.id.wearable_bool);
        boolean activarWearable = sharedPref.getBoolean("wearable",false);
        boolWearable.setChecked(activarWearable);
        boolWearable.setOnCheckedChangeListener(this);
        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        editor.putBoolean("wearable",buttonView.isChecked());
        editor.commit();
    }
}
