package edu.uniandes.aire;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import androidx.navigation.Navigation;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompararFragment extends Fragment implements PermissionsListener {

    String locationProvider = LocationManager.GPS_PROVIDER;
    private FirebaseAnalytics fbAnalytics;
    SharedPreferences sharedPref;
    private Handler customHandler = new Handler();
    private static DecimalFormat df2 = new DecimalFormat(".##");
    PermissionsManager permissionsManager = new PermissionsManager(this);
    Location gpspoint;
    boolean connected = false;
    int gpsStatus = 0;
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            gpspoint = location;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            gpsStatus = status;
        }

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };
    LocationManager locationManager;
    public CompararFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_comparar, container, false);
        getActivity().setTitle("Comparar contaminantes");
        fbAnalytics = FirebaseAnalytics.getInstance(getActivity());
        ImageButton btn = (ImageButton) view.findViewById(R.id.imageView8);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewById = view.findViewById(R.id.desc_pm25);
                if (viewById.getVisibility()==View.GONE){
                    viewById.setVisibility(View.VISIBLE);
                }else {
                    viewById.setVisibility(View.GONE);
                }
            }
        });
        ImageButton btn2 = (ImageButton) view.findViewById(R.id.imageView11);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewById = view.findViewById(R.id.desc_O3);
                if (viewById.getVisibility()==View.GONE){
                    viewById.setVisibility(View.VISIBLE);
                }else {
                    viewById.setVisibility(View.GONE);
                }
            }
        });
        ImageButton btn3 = (ImageButton) view.findViewById(R.id.imageView12);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewById = view.findViewById(R.id.desc_SO2);
                if (viewById.getVisibility()==View.GONE){
                    viewById.setVisibility(View.VISIBLE);
                }else {
                    viewById.setVisibility(View.GONE);
                }
            }
        });
        ImageButton btn4 = (ImageButton) view.findViewById(R.id.imageView13);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewById = view.findViewById(R.id.desc_NO2);
                if (viewById.getVisibility()==View.GONE){
                    viewById.setVisibility(View.VISIBLE);
                }else {
                    viewById.setVisibility(View.GONE);
                }
            }
        });
        ImageButton btn5 = (ImageButton) view.findViewById(R.id.imageView14);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewById = view.findViewById(R.id.desc_CO);
                if (viewById.getVisibility()==View.GONE){
                    viewById.setVisibility(View.VISIBLE);
                }else {
                    viewById.setVisibility(View.GONE);
                }
            }
        });
        ImageButton btn6 = (ImageButton) view.findViewById(R.id.imageView9);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View viewById = view.findViewById(R.id.desc_PM10);
                if (viewById.getVisibility()==View.GONE){
                    viewById.setVisibility(View.VISIBLE);
                }else {
                    viewById.setVisibility(View.GONE);
                }
            }
        });
        ImageButton btn7 = (ImageButton) view.findViewById(R.id.imageButton7);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_historialFragment);
            }
        });
        ImageButton btn8 = (ImageButton) view.findViewById(R.id.imageButton2);
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_historialPM25Fragment);
            }
        });
        ImageButton btn9 = (ImageButton) view.findViewById(R.id.imageButton6);
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_historialCOFragment);
            }
        });
        ImageButton btn10 = (ImageButton) view.findViewById(R.id.imageButton4);
        btn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_historialSO2Fragment);
            }
        });
        ImageButton btn11 = (ImageButton) view.findViewById(R.id.imageButton5);
        btn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_historialNO2Fragment);
            }
        });
        ImageButton btn12 = (ImageButton) view.findViewById(R.id.imageButton3);
        btn12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_historialO3Fragment);
            }
        });
        return view;
    }
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
        customHandler.removeCallbacks(updateData);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!connected){
            Snackbar snackbar = Snackbar
                    .make(getView(), "Reintentar la conexion", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_compararFragment);
                        }
                    });
            snackbar.show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsManager = new PermissionsManager(this);
                permissionsManager.requestLocationPermissions(getActivity());
            }
            locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
            Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
            if (lastKnownLocation!=null){
                gpspoint = lastKnownLocation;
            }
        }
        Location kennedy = new Location("Kennedy");
        kennedy.setLatitude(4.62490);
        kennedy.setLongitude(-74.16135);
        Location puenteAranda = new Location("Puente Aranda");
        puenteAranda.setLatitude(4.63187);
        puenteAranda.setLongitude(-74.11757);
        Location minAmbiente = new Location("MinAmbiente");
        minAmbiente.setLatitude(4.62529);
        minAmbiente.setLongitude(-74.06724);
        Location centroDeAltoRendimiento = new Location("Centro de Alto Rendimiento");
        centroDeAltoRendimiento.setLatitude(4.65837);
        centroDeAltoRendimiento.setLongitude(-74.08400);
        Location guaymaral = new Location("Guaymaral");
        guaymaral.setLatitude(4.78374);
        guaymaral.setLongitude(-74.04417);
        Location suba = new Location("Suba");
        suba.setLatitude(4.76251);
        suba.setLongitude(-74.09343);
        Location sanCristobal = new Location("San Cristobal");
        sanCristobal.setLatitude(4.5725);
        sanCristobal.setLongitude(-74.0836);
        Location cercano = new Location("");
        if (gpspoint != null) {
            float v = gpspoint.distanceTo(kennedy);
            cercano = kennedy;
            if (gpspoint.distanceTo(puenteAranda)<v)
            {
                v=gpspoint.distanceTo(puenteAranda);
                cercano = puenteAranda;
            }
            if (gpspoint.distanceTo(minAmbiente)<v)
            {
                v=gpspoint.distanceTo(minAmbiente);
                cercano = minAmbiente;
            }
            if (gpspoint.distanceTo(centroDeAltoRendimiento)<v)
            {
                v=gpspoint.distanceTo(centroDeAltoRendimiento);
                cercano = centroDeAltoRendimiento;
            }
            if (gpspoint.distanceTo(guaymaral)<v)
            {
                v=gpspoint.distanceTo(guaymaral);
                cercano = guaymaral;
            }
            if (gpspoint.distanceTo(suba)<v)
            {
                v=gpspoint.distanceTo(suba);
                cercano = suba;
            }
            if (gpspoint.distanceTo(sanCristobal)<v)
            {
                v=gpspoint.distanceTo(sanCristobal);
                cercano = sanCristobal;
            }
        }else {
            cercano = kennedy;
        }
        ConnectivityManager cm =(ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null&&activeNetwork.isConnected()) {
            connected = true;
            Bundle event = new Bundle();
//            AsyncTask<String, Void, String[]> execute = new RetrieveValues().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,cercano.getProvider());
            AsyncTask<String, Void, String[]> execute = new RetrieveValues(new OnEventListener() {
                @Override
                public void onSuccess(String[] strings) {
                    TextView textoPM10 = (TextView)getView().findViewById(R.id.pm10_value);
                    textoPM10.setText(strings[2]);
                    if (strings[2].compareTo("null")==0){
                        CardView cardView = (CardView)getView().findViewById(R.id.compararCardView7);
                        cardView.setVisibility(View.GONE);
                    }
                    if (strings[2].compareTo("null")!=0){
                        event.putDouble("pm10_level",Double.parseDouble(strings[2]));
                    }
                    TextView textoPM25 = (TextView)getView().findViewById(R.id.pm25_value);
                    textoPM25.setText(strings[3]);
                    if (strings[3].compareTo("null")==0){
                        CardView cardView = (CardView)getView().findViewById(R.id.compararCardView2);
                        cardView.setVisibility(View.GONE);
                    }
                    if (strings[3].compareTo("null")!=0) {
                        event.putDouble("pm25_level", Double.parseDouble(strings[3]));
                    }
                    TextView textoO3 = (TextView)getView().findViewById(R.id.o3_value);
                    textoO3.setText(strings[4]);
                    if (strings[4].compareTo("null")==0){
                        CardView cardView = (CardView)getView().findViewById(R.id.compararCardView3);
                        cardView.setVisibility(View.GONE);
                    }
                    if (strings[4].compareTo("null")!=0) {
                        event.putDouble("O3_level", Double.parseDouble(strings[4]));
                    }
                    TextView textoNO2 = (TextView)getView().findViewById(R.id.no2_value);
                    textoNO2.setText(strings[5]);
                    if (strings[5].compareTo("null")==0){
                        CardView cardView = (CardView)getView().findViewById(R.id.compararCardView5);
                        cardView.setVisibility(View.GONE);
                    }
                    if (strings[5].compareTo("null")!=0) {
                        event.putDouble("NO2_level", Double.parseDouble(strings[5]));
                    }
                    TextView textoCO = (TextView)getView().findViewById(R.id.co_value);
                    textoCO.setText(strings[7]);
                    if (strings[7].compareTo("null")==0){
                        CardView cardView = (CardView)getView().findViewById(R.id.compararCardView6);
                        cardView.setVisibility(View.GONE);
                    }
                    if (strings[7].compareTo("null")!=0) {
                        event.putDouble("CO_level", Double.parseDouble(strings[7]));
                    }
                    TextView textoSO2 = (TextView)getView().findViewById(R.id.so2_value);
                    textoSO2.setText(strings[8]);
                    if (strings[8].compareTo("null")==0){
                        CardView cardView = (CardView)getView().findViewById(R.id.compararCardView4);
                        cardView.setVisibility(View.GONE);
                    }
                    if (strings[8].compareTo("null")!=0) {
                        event.putDouble("SO2_level", Double.parseDouble(strings[8]));
                    }
                    TextView textoLocalidad = (TextView)getView().findViewById(R.id.comparar_tiempo);
                    textoLocalidad.setText(strings[0]+"-"+strings[1]);
                }
            }).execute(cercano.getProvider());
            TextView textoLocalidad = (TextView)getView().findViewById(R.id.comparar_localidad);
            textoLocalidad.setText(cercano.getProvider());
            event.putString("last_station", cercano.getProvider());
            fbAnalytics.logEvent("comparar_contaminantes", event);
        }else {
            connected = false;
            TextView textoPM10 = (TextView)getView().findViewById(R.id.pm10_value);
            textoPM10.setText("N/D");
            TextView textoPM25 = (TextView)getView().findViewById(R.id.pm25_value);
            textoPM25.setText("N/D");
            TextView textoO3 = (TextView)getView().findViewById(R.id.o3_value);
            textoO3.setText("N/D");
            TextView textoNO2 = (TextView)getView().findViewById(R.id.no2_value);
            textoNO2.setText("N/D");
            TextView textoCO = (TextView)getView().findViewById(R.id.co_value);
            textoCO.setText("N/D");
            TextView textoSO2 = (TextView)getView().findViewById(R.id.so2_value);
            textoSO2.setText("N/D");
        }
//        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
//        boolean activarWearable = sharedPref.getBoolean("wearable",false);
//        int interpetVisibility;
//        if (activarWearable){
//            interpetVisibility = View.VISIBLE;
//            customHandler.postAtTime(updateData,0);
//        } else {
//            interpetVisibility = View.INVISIBLE;
//        }
//        TextView textoTituloWearable = (TextView)getView().findViewById(R.id.textView2);
//        textoTituloWearable.setVisibility(interpetVisibility);
//        View viewById = getView().findViewById(R.id.frameLayout4);
//        viewById.setVisibility(interpetVisibility);
    }
    private Runnable updateData = new Runnable() {
        @Override
        public void run() {
            TextView textoPM10 = (TextView)getView().findViewById(R.id.pm10_2_value);
            textoPM10.setText(df2.format(Math.random()*10));
            TextView textoPM25 = (TextView)getView().findViewById(R.id.pm25_2_value);
            textoPM25.setText(df2.format(Math.random()*10));
            TextView textoO3 = (TextView)getView().findViewById(R.id.o3_2_value);
            textoO3.setText(df2.format(Math.random()*10));
            TextView textoNO2 = (TextView)getView().findViewById(R.id.no2_2_value);
            textoNO2.setText(df2.format(Math.random()*10));
            TextView textoCO = (TextView)getView().findViewById(R.id.co_2_value);
            textoCO.setText(df2.format(Math.random()*10));
            TextView textoSO2 = (TextView)getView().findViewById(R.id.so2_2_value);
            textoSO2.setText(df2.format(Math.random()*10));
            customHandler.postDelayed(updateData,10000);
        }
    };
}
