package edu.uniandes.aire;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import java.util.List;

import androidx.navigation.Navigation;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistorialNO2Fragment extends Fragment implements PermissionsListener {


    boolean connected = false;
    Location gpspoint;
    LocationManager locationManager;
    PermissionsManager permissionsManager = new PermissionsManager(this);
    String locationProvider = LocationManager.GPS_PROVIDER;
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            gpspoint = location;
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };
    public HistorialNO2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_historial_no2, container, false);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {

    }
    @Override
    public void onResume() {
        super.onResume();
        if (!connected){
            Snackbar snackbar = Snackbar
                    .make(getView(), "Reintentar la conexion", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment).navigate(R.id.action_global_detallesFragment);
                        }
                    });
            snackbar.show();
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsManager = new PermissionsManager(this);
                permissionsManager.requestLocationPermissions(getActivity());
            }
            locationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);
            Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
            if (lastKnownLocation!=null){
                gpspoint = lastKnownLocation;
            }
        }
        ConnectivityManager cm =(ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork!=null&&activeNetwork.isConnected()) {
            connected = true;
            AsyncTask<String, Void, String[]> kennedy = new RetrieveHistory(new OnEventListener() {
                @Override
                public void onSuccess(String[] strings) {
                    GraphView graph = (GraphView) getView().findViewById(R.id.graph);
                    LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[] {
                            new DataPoint(-4, Double.parseDouble(strings[5])),
                            new DataPoint(-3, Double.parseDouble(strings[6])),
                            new DataPoint(-2, Double.parseDouble(strings[7])),
                            new DataPoint(-1, Double.parseDouble(strings[8])),
                            new DataPoint(0, Double.parseDouble(strings[9]))
                    });
                    graph.addSeries(series);
                }
            }).execute("NO2");
        }else {
            connected = false;
        }
    }

}
